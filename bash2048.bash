#!/usr/bin/bash

# Woefully incomplete!

game_field="0 0 0 0\n0 0 0 0\n0 0 0 0\n0 0 0 0"
game_score=0
game_turns=0
function print_base() {
    clear
    printf '\033[2;3HScore:'
    printf '\033[3;3HTurns:'
    printf '\033[5;3H+----+----+----+----+'
    printf '\033[6;3H|    |    |    |    |'
    printf '\033[7;3H+----+----+----+----+'
    printf '\033[8;3H|    |    |    |    |'
    printf '\033[9;3H+----+----+----+----+'
    printf '\033[10;3H|    |    |    |    |'
    printf '\033[11;3H+----+----+----+----+'
    printf '\033[12;3H|    |    |    |    |'
    printf '\033[13;3H+----+----+----+----+'
    printf '\033[15;3HControls: arrows'
    printf '\033[16;7HQuit: Q\n\n'
}
function add_number() {
    return 1
}
function print_field() {
    printf "\033[2,9%s" "${game_score}"
    printf "\033[3,9%s" "${game_turns}"
    l=6
    for r in "$game_field"; do
        for c in {1..4}; do
            n=$(echo "$r" | awk "{print \$$c}")
            printf "\033[${l};4H%s" "$n"
        done
        l=$((l+2))
    done
}
function reverse_field() {
    return 1
}
function invert_field() {
    return 1
}
function shift_left() {
    return 1
}
function shift_right() {
    reverse_field
    shift_left
    reverse_field
}
function shift_up() {
    invert_field
    shift_left
    invert_field
}
function shift_down() {
    invert_field
    reverse_field
    shift_left
    reverse_field
    invert_field
}
function main() {
    print_base
    while true; do
        print_field
        read -rn1 c
        case "$c" in
            (Q)
            (q)
            (X)
            (x)
            (.)
                printf 'Buh-bye!\n'
                break
                ;;
            (W)
            (w)
            (K)
            (k)
                shift_up
                ;;
            (A)
            (a)
            (H)
            (h)
                shift_left
                ;;
            (S)
            (s)
            (J)
            (j)
                shift_down
                ;;
            (D)
            (d)
            (L)
            (l)
                shift_right
                ;;
            ($'\033')
                read -t.0001 -n2 r
                case "$r" in
                    ('[A') shift_up ;;
                    ('[B') shift_down ;;
                    ('[C') shift_right ;;
                    ('[D') shift_left ;;
                esac
        esac
    done
}
main
