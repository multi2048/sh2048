#!/usr/bin/env fish

#This one does NOT yet work

set --local r1 0 0 0 0
set --local r2 0 0 0 0
set --local r3 0 0 0 0
set --local r4 0 0 0 0
set --local board r1 r2 r3 r4
set --local score 0 0

function main
    paintboard
    # I need to figure out how to listen for key presses.
    # Ideally, I'd like to display the state, pause the script until a "valid"
    # key is pressed, then continmue to run, doing the corresponting action,
    # and loop back around to display the new state and again listen for a
    # keypress. Only exiting the loop once a specified key is pressed.
end

function paintboard
    clear
    # Need to paint the board
end

function paintstatus
    # commandline -f repaint
    # Need to paint he current state of the game
end

function mirror
    set --local tempr 0 0 0 0
    for r in (seq 4)
        for c in (seq 4)
            set tempr[(math - $c)) $$board[$r][$c]
        end
        set $board[$r] $tempr
    end
end

function invert
    set --local temp 0
    for r in (seq 3)
        for c in (seq 2 4)
            set temp $$board[$r][$c]
            set $board[$r][$c] $$board[$c][$r]
            set $board[$c][$r] $temp
        end
    end
end

function addnum
    set --local count 0
    set --local num 2
    set --local cells ""
    set --local choice "00"
    for r in (seq 4)
        for c in (seq 4)
            if test $$board[$r][$c] -eq 0
                set cells $cells "$r$c"
                set count (math $count + 1)
            end
        end
    end
    set cells $cells[2..]
    set choice $cells[(random 1 $count)]
    if test (random 1 10) -eq 1
        set num 4
    end
    set board[(string sub --length 1 $choice)][(string sub --start 2 $choice)] $num
end

function pushlt
    set --local changed 0
    set --local tempc 1
    for r in (seq 4)
        set tempc 1
        for c in (seq 2 4)
            while test $tempc -lt $c
                if test $$board[$r][$c] -gt 0
                    set $board[$r][$tempc] $$board[$r][$c]
                    set $board[$r][$c] 0
                    set changed 1
                    break
                else if test $$board[$r][$tempc] -eq $$board[$r][$c]
                    set $board[$r][$tempc] (math $$board[$r][$tempc] * 2)
                    set $board[$r][$c] 0
                    set score[0] (math $score[0] + $$board[$r][$tempc])
                    set changed 1
                    set tempc (math $tempc + 1)
                    break
                else
                    set tempc (math $tempc + 1)
                end
            end
        end
    end
    if test changed -eq 1
        addnum
        set score[1] (math $score[1] + 1)
    end
end

function pushrt
    mirror
    pushlt
    mirror
end

function pushup
    invert
    pushlt
    invert
end

function pushdn
    invert
    pushrt
    invert
end
